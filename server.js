const http = require("http");

const server = http.createServer((req, res) => {
  res.writeHead(200);
  res.end("Web server");
});

const port = 3000;
server.listen(port, () => {
  console.log(`Server is up on - http://localhost:${port}`);
});
